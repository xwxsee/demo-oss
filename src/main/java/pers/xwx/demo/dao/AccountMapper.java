/**
 * 
 */
package pers.xwx.demo.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import pers.xwx.demo.entity.Account;

/**
 * @author user
 *
 */
@Mapper
public interface AccountMapper {
	
	@Select("SELECT * FROM account WHERE id = #{id}")
	public Account findById(@Param("id") String id);
	
	@Insert({"insert into account(id, name, sex) values(#{id}, #{name}, #{sex})"})
	public int insert(Account account);
	
	@Update({"update account set name = #{name}, sex = #{sex} where id = #{id}"})
	public int update(Account account);

}

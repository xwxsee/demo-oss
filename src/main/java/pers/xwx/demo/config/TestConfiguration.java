/**
 * 
 */
package pers.xwx.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author user
 *
 */
@Configuration
public class TestConfiguration {

	@Bean
	public TestFilter testFilter() {
		return new TestFilter();
	}
	
}

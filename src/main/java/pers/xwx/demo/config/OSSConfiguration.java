/**
 * 
 */
package pers.xwx.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.aliyun.oss.OSSClient;

import pers.xwx.demo.property.OSSProperties;

/**
 * oss configuration
 * 
 * @author wenxing
 *
 */
@Configuration
public class OSSConfiguration {
	
	/**
	 * inject oss properties
	 */
	@Autowired
	private OSSProperties ossProps;
	
	/**
	 * regist oss client bean
	 * @return
	 */
	@Bean
	public OSSClient ossClient() {
		return new OSSClient(ossProps.getEndpoint(), ossProps.getAccessKey(), ossProps.getAccessSecret());
	}

}

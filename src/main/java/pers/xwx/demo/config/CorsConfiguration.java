/**
 * 
 */
package pers.xwx.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @author user
 *
 */
@Configuration 
public class CorsConfiguration extends WebMvcConfigurerAdapter { 
 
    @Override 
    public void addCorsMappings(CorsRegistry registry) { 
        registry.addMapping("/**") 
                .allowedOrigins("*") 
                .allowedHeaders("AUTH_TOKEN", "content-type")
                .allowCredentials(true) 
                .allowedMethods("GET", "POST", "DELETE", "PUT") 
                .maxAge(3600); 
    } 
 
}

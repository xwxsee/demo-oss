/**
 * 
 */
package pers.xwx.demo.config;

import com.alibaba.druid.filter.FilterEventAdapter;
import com.alibaba.druid.proxy.jdbc.StatementProxy;

/**
 * @author user
 *
 */
public class TestFilter extends FilterEventAdapter {
	
	@Override
    protected void statementExecuteAfter(StatementProxy statement, String sql, boolean result) {
        System.out.println(statement.getLastExecuteSql());
        super.statementExecuteAfter(statement, sql, result);
    }

}

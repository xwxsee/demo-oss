/**
 * 
 */
package pers.xwx.demo.service;

import pers.xwx.demo.entity.Account;

/**
 * @author user
 *
 */
public interface IAccountService {
	
	public Account getAccountById(String id);
	
	public int insert(Account account);
	
	public int update(Account account);

}

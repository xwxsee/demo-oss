/**
 * 
 */
package pers.xwx.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pers.xwx.demo.dao.AccountMapper;
import pers.xwx.demo.entity.Account;

/**
 * @author user
 *
 */
@Service
public class AccountServiceImpl implements IAccountService {
	
	@Autowired
	private AccountMapper accountMapper;

	/* (non-Javadoc)
	 * @see pers.xwx.demo.service.IAccountService#getAccountById(java.lang.String)
	 */
	@Override
	public Account getAccountById(String id) {
		return accountMapper.findById(id);
	}

	@Override
	public int insert(Account account) {
		return accountMapper.insert(account);
	}

	@Override
	public int update(Account account) {
		return accountMapper.update(account);
	}

}

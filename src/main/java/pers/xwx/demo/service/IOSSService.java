/**
 * 
 */
package pers.xwx.demo.service;

import com.aliyun.oss.model.CompleteMultipartUploadResult;

import pers.xwx.demo.dto.RequestCompleteUploadDTO;
import pers.xwx.demo.dto.RequestMultipartUploadDataDTO;
import pers.xwx.demo.dto.ResultMultipartUploadDTO;
import pers.xwx.demo.dto.ResultPartETagDTO;
import pers.xwx.demo.dto.ResultUploadInfoDTO;

/**
 * @author user
 *
 */
public interface IOSSService {
	
	/**
	 * 生成临时下载url
	 * 
	 * @param bucketName
	 * @param objectKey
	 * @return
	 */
	public String getDownloadSignatureUrl(String bucketName, String objectKey);
	
	/**
	 * 生成普通上传签名
	 * 
	 * @param bucketName
	 * @param fileName
	 * @return
	 */
	public ResultUploadInfoDTO getUploadSignature(String bucketName, String fileName);
	
	/**
	 * 初始化分片上传
	 * 
	 * @param bucketName
	 * @param fileName
	 * @return
	 */
	public ResultMultipartUploadDTO initMultipartUpload(String bucketName, String fileName);
	
	/**
	 * 上传分片数据
	 * @param data
	 * @return
	 */
	public ResultPartETagDTO dataMultipartUpload(RequestMultipartUploadDataDTO data);
	
	/**
	 * 完成分片上传
	 * @param data
	 * @return
	 */
	public CompleteMultipartUploadResult getMultipleUploadComplete(RequestCompleteUploadDTO data);

}

/**
 * 
 */
package pers.xwx.demo.dto;

/**
 * @author user
 *
 */
public class ResultPartETagDTO {
	
	private int partNum;
	
	private String partEtag;

	public int getPartNum() {
		return partNum;
	}

	public void setPartNum(int partNum) {
		this.partNum = partNum;
	}

	public String getPartEtag() {
		return partEtag;
	}

	public void setPartEtag(String partEtag) {
		this.partEtag = partEtag;
	}

}

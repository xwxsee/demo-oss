/**
 * 
 */
package pers.xwx.demo.dto;

import java.util.List;

/**
 * @author user
 *
 */
public class RequestCompleteUploadDTO {
	
	private String uploadId;
	
	private String ossBucketName;
	
	private String ossObjectName;
	
	private List<RequestPartETagDTO> partETags;

	public String getUploadId() {
		return uploadId;
	}

	public void setUploadId(String uploadId) {
		this.uploadId = uploadId;
	}

	public String getOssBucketName() {
		return ossBucketName;
	}

	public void setOssBucketName(String ossBucketName) {
		this.ossBucketName = ossBucketName;
	}

	public String getOssObjectName() {
		return ossObjectName;
	}

	public void setOssObjectName(String ossObjectName) {
		this.ossObjectName = ossObjectName;
	}

	public List<RequestPartETagDTO> getPartETags() {
		return partETags;
	}

	public void setPartETags(List<RequestPartETagDTO> partETags) {
		this.partETags = partETags;
	}

}

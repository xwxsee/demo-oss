/**
 * 
 */
package pers.xwx.demo.dto;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author user
 *
 */
public class RequestMultipartUploadDataDTO {
	
	private String bucketName;
	
	private String objectName;
	
	private String uploadId;
	
	private int partNum;
	
	private MultipartFile file;

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public String getUploadId() {
		return uploadId;
	}

	public void setUploadId(String uploadId) {
		this.uploadId = uploadId;
	}

	public int getPartNum() {
		return partNum;
	}

	public void setPartNum(int partNum) {
		this.partNum = partNum;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

}

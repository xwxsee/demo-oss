/**
 * 
 */
package pers.xwx.demo.dto;

/**
 * @author user
 *
 */
public class ResultMultipartUploadDTO {
	
	private String objectName;
	
	private String bucketName;
	
	private String fileName;
	
	private String uploadId;

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getUploadId() {
		return uploadId;
	}

	public void setUploadId(String uploadId) {
		this.uploadId = uploadId;
	}

}

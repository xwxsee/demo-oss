/**
 * 
 */
package pers.xwx.demo.dto;

/**
 * @author user
 *
 */
public class RequestPartETagDTO {
	
	private int partNumber;
	
	private String partTag;

	public int getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(int partNumber) {
		this.partNumber = partNumber;
	}

	public String getPartTag() {
		return partTag;
	}

	public void setPartTag(String partTag) {
		this.partTag = partTag;
	}

}

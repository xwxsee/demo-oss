/**
 * 
 */
package pers.xwx.demo.dto;

/**
 * @author user
 *
 */
public class ResultUploadInfoDTO {
	
	private String date;
	
	private String signature;
	
	private String bucketName;
	
	private String objectName;
	
	private String fileName;
	
	private String dispositionValue;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getObjectName() {
		return objectName;
	}

	public void setObjectName(String objectName) {
		this.objectName = objectName;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDispositionValue() {
		return dispositionValue;
	}

	public void setDispositionValue(String dispositionValue) {
		this.dispositionValue = dispositionValue;
	}

}

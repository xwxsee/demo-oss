/**
 * 
 */
package pers.xwx.demo.property;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * oss configuration properties
 * 
 * @author wenxing
 *
 */
@Component
@ConfigurationProperties("oss.config")
public class OSSProperties {
	
	/**
	 * oss access key
	 */
	private String accessKey;
	/**
	 * oss access secret
	 */
	private String accessSecret;
	/**
	 * oss endpoint
	 */
	private String endpoint;
	
	/**
	 * download url expired time
	 */
	private int downloadExpireTime;

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getAccessSecret() {
		return accessSecret;
	}

	public void setAccessSecret(String accessSecret) {
		this.accessSecret = accessSecret;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public int getDownloadExpireTime() {
		return downloadExpireTime;
	}

	public void setDownloadExpireTime(int downloadExpireTime) {
		this.downloadExpireTime = downloadExpireTime;
	}

}

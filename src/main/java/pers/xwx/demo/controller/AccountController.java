/**
 * 
 */
package pers.xwx.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pers.xwx.demo.entity.Account;
import pers.xwx.demo.service.IAccountService;

/**
 * @author user
 *
 */
@RestController
@RequestMapping("/demo/v1")
public class AccountController {
	
	@Autowired
	private IAccountService accountService;
	
	@GetMapping("/accounts/{id}")
	public ResponseEntity<Account> getAccount(@PathVariable String id) {
		return new ResponseEntity<>(accountService.getAccountById(id), HttpStatus.OK);
	}
	
	@PostMapping("/accounts")
	public ResponseEntity<String> postAccount(@RequestBody Account account) {
		accountService.insert(account);
		return new ResponseEntity<>("success", HttpStatus.OK);
	}
	
	@PutMapping("/accounts/{id}")
	public ResponseEntity<String> postAccount(@PathVariable String id, @RequestBody Account account) {
		account.setId(id);
		accountService.update(account);
		return new ResponseEntity<>("success", HttpStatus.OK);
	}

}

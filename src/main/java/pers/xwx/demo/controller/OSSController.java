/**
 * 
 */
package pers.xwx.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aliyun.oss.model.CompleteMultipartUploadResult;

import pers.xwx.demo.dto.RequestCompleteUploadDTO;
import pers.xwx.demo.dto.RequestMultipartUploadDataDTO;
import pers.xwx.demo.dto.ResultMultipartUploadDTO;
import pers.xwx.demo.dto.ResultPartETagDTO;
import pers.xwx.demo.dto.ResultUploadInfoDTO;
import pers.xwx.demo.service.IOSSService;

/**
 * @author user
 *
 */
@RestController
@RequestMapping("/oss/v1")
public class OSSController {

	@Autowired
	private IOSSService ossService;

	/**
	 * generate the download url
	 * 
	 * @param bucketName
	 * @param objectKey
	 * @return
	 */
	@GetMapping("/signature/download")
	public ResponseEntity<String> getDownloadSignatureUrl(@RequestParam(required = false) String bucketName,
			@RequestParam String objectKey) {
		String signature = ossService.getDownloadSignatureUrl(bucketName, objectKey);

		return new ResponseEntity<>(signature, signature == null ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.OK);
	}

	/**
	 * generate the normal upload signature
	 * 
	 * @param fileName
	 * @return
	 */
	@GetMapping("/signature/upload")
	public ResponseEntity<ResultUploadInfoDTO> getUploadSignature(@RequestParam(required = false) String bucketName,
			@RequestParam String fileName) {
		ResultUploadInfoDTO result = ossService.getUploadSignature(bucketName, fileName);

		return new ResponseEntity<>(result, result == null ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.OK);
	}

	/**
	 * 初始化分片上传
	 * 
	 * @param bucketName
	 * @param fileName
	 * @return
	 */
	@GetMapping("/multiple_upload/init")
	public ResponseEntity<ResultMultipartUploadDTO> initMultipartUpload(
			@RequestParam(required = false) String bucketName, @RequestParam String fileName) {
		ResultMultipartUploadDTO result = ossService.initMultipartUpload(bucketName, fileName);

		return new ResponseEntity<>(result, result == null ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.OK);
	}

	/**
	 * 上传分片数据
	 * 
	 * @param data
	 * @return
	 */
	@PostMapping("/multiple_upload/data")
	public ResponseEntity<ResultPartETagDTO> dataMultipartUpload(RequestMultipartUploadDataDTO data) {
		ResultPartETagDTO result = ossService.dataMultipartUpload(data);

		return new ResponseEntity<>(result, result == null ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.OK);
	}

	/**
	 * 上传完成回调
	 * 
	 * @param data
	 * @return
	 */
	@PostMapping("/multiple_upload/complete")
	public ResponseEntity<CompleteMultipartUploadResult> getMultipleUploadComplete(
			@RequestBody RequestCompleteUploadDTO data) {
		CompleteMultipartUploadResult result = ossService.getMultipleUploadComplete(data);
		
		return new ResponseEntity<>(result, result == null ? HttpStatus.INTERNAL_SERVER_ERROR : HttpStatus.OK);
	}

}
